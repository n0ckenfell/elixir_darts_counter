defmodule ElixirDartsTest do
  use ExUnit.Case
  doctest ElixirDarts

  alias ElixirDarts.Game

  defp no_score(game_pid) do
    ElixirDarts.throw(game_pid, { 0,0 })
    ElixirDarts.throw(game_pid, { 0,0 })
    ElixirDarts.throw(game_pid, { 0,0 })
  end

  defp one_hundret_eighty(game_pid) do
    ElixirDarts.throw(game_pid, { 20,3 })
    ElixirDarts.throw(game_pid, { 20,3 })
    ElixirDarts.throw(game_pid, { 20,3 })
  end

  defp nine_dart_finish(game_pid) do
    ElixirDarts.throw(game_pid, { 20,3 })
    ElixirDarts.throw(game_pid, { 19,3 })
    ElixirDarts.throw(game_pid, { 12,2 })
  end

  defp bob_throws(game_pid) do
    ElixirDarts.throw(game_pid, { 10, 2 })
    ElixirDarts.throw(game_pid, { 20, 1 })
    ElixirDarts.throw(game_pid, { 5, 3 })
  end

  defp frank_throws(game_pid) do
    ElixirDarts.throw(game_pid, { 1, 1 })
    ElixirDarts.throw(game_pid, { 2, 1 })
    ElixirDarts.throw(game_pid, { 20, 3 })
  end

  test "sart a game" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    assert ElixirDarts.game(game_pid) == { {"Bob",[]}, {"Frank",[]}, "Bob" }
  end

  test "Bob ElixirDarts.throws one dart" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    ElixirDarts.throw(game_pid, { 10, 2 })
    assert ElixirDarts.game(game_pid) == { {"Bob",[{ 10, 2}]}, {"Frank",[]}, "Bob" }
  end

  test "Bob ElixirDarts.throws two darts" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    ElixirDarts.throw(game_pid, { 10, 2 })
    ElixirDarts.throw(game_pid, { 20, 1 })
    assert ElixirDarts.game(game_pid) == { {"Bob",[{ 20, 1}, { 10, 2}]}, {"Frank",[]}, "Bob" }
  end

  test "Bob ElixirDarts.throws three darts" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    bob_throws(game_pid)
    assert ElixirDarts.game(game_pid) == { {"Bob",[{ 5, 3}, { 20, 1}, { 10, 2}]}, {"Frank",[]}, "Frank" }
  end

  test "Frank ElixirDarts.throws first dart after Bob ElixirDarts.thrown his three darts" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    bob_throws(game_pid)

    ElixirDarts.throw(game_pid, { 1, 1 })
    assert ElixirDarts.game(game_pid) == { {"Bob",[{ 5, 3}, { 20, 1}, { 10, 2}]}, {"Frank",[{1,1}]}, "Frank" }
  end

  test "Frank ElixirDarts.throws second dart after Bob ElixirDarts.thrown his three darts" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    bob_throws(game_pid)

    ElixirDarts.throw(game_pid, { 1, 1 })
    ElixirDarts.throw(game_pid, { 2, 1 })
    assert ElixirDarts.game(game_pid) == { {"Bob",[{ 5, 3}, { 20, 1}, { 10, 2}]}, {"Frank",[{2,1},{1,1}]}, "Frank" }
  end

  test "Frank ElixirDarts.throws third dart after Bob ElixirDarts.thrown his three darts" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    bob_throws(game_pid)
    frank_throws(game_pid)
    assert ElixirDarts.game(game_pid) == { {"Bob",[{ 5, 3}, { 20, 1}, { 10, 2}]}, {"Frank",[{20,3},{2,1},{1,1}]}, "Bob" }
  end

  test "Bob ElixirDarts.throws a 9-darter" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")

    one_hundret_eighty(game_pid)
    frank_throws(game_pid)

    one_hundret_eighty(game_pid)
    frank_throws(game_pid)

    nine_dart_finish(game_pid)

    assert ElixirDarts.game(game_pid) == {
      { "Bob", [ 
                 {12,2},{19,3},{20,3},
                 {20,3},{20,3},{20,3},
                 {20,3},{20,3},{20,3},
               ]
      },
      { "Frank", [
                 {20,3},{2,1},{1,1},
                 {20,3},{2,1},{1,1},
                 ]
      },
      { :winner, "Bob" }
    }

  end

  test "Frank ElixirDarts.throws a 9-darter" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    bob_throws(game_pid)
    one_hundret_eighty(game_pid)
    bob_throws(game_pid)
    one_hundret_eighty(game_pid)
    bob_throws(game_pid)
    nine_dart_finish(game_pid)

    assert ElixirDarts.game(game_pid) == {
      { "Bob", [ 
                 {5,3},{20,1},{10,2},
                 {5,3},{20,1},{10,2},
                 {5,3},{20,1},{10,2},
               ]
      },
      { "Frank", [
                   {12,2},{19,3},{20,3},
                   {20,3},{20,3},{20,3},
                   {20,3},{20,3},{20,3},
                 ]
      },
      { :winner, "Frank" }
    }
  end

  test "Bob busts a 9-darter twice and then finish with all edge-cases" do
    {:ok, game_pid} = Game.start_link("Bob", "Frank")
    one_hundret_eighty(game_pid) # 180
    no_score(game_pid) # Frank

    one_hundret_eighty(game_pid) # 360
    no_score(game_pid) # Frank

    one_hundret_eighty(game_pid) # 540, bust with last dart
    no_score(game_pid) # Frank

    ElixirDarts.throw(game_pid,{20,3}) # bust again with first dart
    no_score(game_pid) # Frank

    ElixirDarts.throw(game_pid,{20,1}) # bust with 1 rest
    no_score(game_pid) # Frank

    ElixirDarts.throw(game_pid,{1,1})
    ElixirDarts.throw(game_pid,{20,1}) # bust with zero rest but not a double
    no_score(game_pid) # Frank
    
    ElixirDarts.throw(game_pid,{10,2}) # now finish!

    assert ElixirDarts.game(game_pid) == {
      { "Bob", [
                               {10,2}, # hooray!
                 {0,0}, {20,0}, {1,1}, #1 + 20 but last is no double, busted
                 {0,0}, {0,0}, {20,0}, #20, 1 rest, busted
                 {0,0}, {0,0}, {20,0}, #60, busted
                 {20,0},{20,3},{20,3}, #180 busted last dart doesn't count, 21 rest
                 {20,3},{20,3},{20,3}, #180
                 {20,3},{20,3},{20,3}, #180
               ]
      },
      { "Frank", [
                   {0,0},{0,0},{0,0},
                   {0,0},{0,0},{0,0},
                   {0,0},{0,0},{0,0},
                   {0,0},{0,0},{0,0},
                   {0,0},{0,0},{0,0},
                   {0,0},{0,0},{0,0}
                 ]
      },
      {:winner, "Bob"}
    }
  end

end
