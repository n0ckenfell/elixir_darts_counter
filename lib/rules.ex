defmodule ElixirDarts.Rules do

  # API

  def count_for_current_player({{current_player,d1},{p2,d2}, current_player}, current_player, dart ) do
    {
      {current_player,[dart|d1]},
      {p2,d2}, 
      current_player
    }
  end
  def count_for_current_player({{p1,d1},{current_player,d2}, current_player}, current_player, dart ) do
    {
      {p1,d1},
      {current_player,[dart|d2]},
      current_player
    }
  end

  def dedect_bust({{p1,d1}, {p2,d2}, current_player}) do
    {{p1,bust(d1)}, {p2,bust(d2)}, current_player}
  end

  def set_current_player( {{p1,d1}, {p2,d2}, _cp} ) do
    new_player = count_darts([d1,d2]) |> switch_player( p1, p2 )
    {{p1,d1},{p2,d2},new_player}
  end

  def detect_win({{p1,d1}, {p2,d2}, cp}) do
    case sum_darts(d1,d2) do
      [501,_] -> {{p1,d1}, {p2,d2}, { :winner, p1 }}
      [_,501] -> {{p1,d1}, {p2,d2}, { :winner, p2 }}
      _ -> {{p1,d1}, {p2,d2}, cp}
    end
  end

  # Implementation 

  # d is a list of tuples [{value, multiplier},....]
  defp bust(d), do: sum_darts(d) |> bust(d)
  # last throw isn't a double
  defp bust(s,[{v,m}|d]) when s == 501 and m != 2, do: invalid_throw(d,v)
  # rest 1 is a bust
  defp bust(s,[{v,_}|d]) when s == 500, do: invalid_throw(d,v)
  # busted over 501
  defp bust(s,[{v,_}|d]) when s > 501, do: invalid_throw(d,v)
  # valid throw
  defp bust(_,d), do: d

  defp invalid_throw(d,value), do: [{value,0}|d] |> fill_record
  defp fill_record(d) do
    Enum.reduce_while(1..3, d, fn _i, acc ->
      if rem(Enum.count(acc),3) != 0, do: {:cont, [{0,0}|acc]}, else: {:halt, acc}
    end)
  end

  defp switch_player( [hits_one, hits_two], p1, p2 ) when hits_one > hits_two do
    if (rem(hits_one,3) == 0), do: p2, else: p1
  end
  defp switch_player( [_hits_one, _hits_two], p1, _p2 ), do: p1



  defp sum_darts(d), do: Enum.reduce(d, 0, fn({v,m}, sum) -> sum + v*m end)
  defp sum_darts(d1,d2), do: [sum_darts(d1),sum_darts(d2)]
  defp count_darts(list_of_darts), do: Enum.map(list_of_darts, &Enum.count(&1))
end

