defmodule ElixirDarts do
  @moduledoc """
  The elixir darts counter has the following tuple to 
  define the state of a game

  `{  player1, player2, current_player }`

  player1 and player2 are a tuple of the form `{ name, [dart,dart,...] }`

  and finaly dart is a tuple of `{ value, multiplier }`.

  Example:

      { 
        { "Phil the Power Taylor",    [ {20,3}, {20,3}, {20,3}, {20,3} ] },
        { "Mensur the Gent Suljovic", [ {20,3}, {20,3}, {20,1} ],
        "Phil the Power Taylor"
      }

  This example can be read as:

  Phil started with a 180,
  Then Mensur hit the single 20, followed by two triple 20s
  And now it is Phil's turn again and he started just with another triple 20

  Note, the list of darts thrown has to be read from back to front because
  in Erlang/Elixir we prepend to lists instead of appending ;-)


  Throwing a dart by calling `Game.throw_dart( {value, multiplier} )`

  The Game will take care that the dart is counted for the current
  player and being validated.

  Once a player finished to zero with a double, the sate will return a 
  winner instead of the current player.

      { player1, player2, { :winner, "Whoever it is" } }

  Have Fun!
  """

  @doc"""
    Return the state of the game running at PID `pid`

    ## Example

    iex> {:ok, pid} = ElixirDarts.Game.start_link("Phil", "Mensur")
    iex> ElixirDarts.game(pid)
    { { "Phil", [] }, { "Mensur", [] }, "Phil" }
    
  """
  def game(pid) do
    ElixirDarts.Game.game(pid)
  end


  @doc"""
  Throw a dart

    ## Example
    iex> {:ok, pid} = ElixirDarts.Game.start_link("Phil", "Mensur")
    iex> ElixirDarts.throw(pid, {20, 1} )
    iex> ElixirDarts.throw(pid, {20, 3} )
    iex> ElixirDarts.game(pid)
    { { "Phil", [{20,3}, {20,1}] }, { "Mensur", [] }, "Phil" }
  """
  def throw(pid, dart ) do
    ElixirDarts.Game.throw_dart(pid, dart)
  end

end
