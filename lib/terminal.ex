defmodule ElixirDarts.Terminal do

  def main(_args) do
    IO.puts "ELIXIR DARTS V0.1"
    main_loop()
  end

  defp main_loop do
    p1 = IO.gets "Please enter name of player one: "
    p2 = IO.gets "Please enter name of player two: "

    start_game(p1,p2)

    answer = IO.gets "Play again? (Y/n): "
    if answer == "n\n", do: goodbye(), else: main_loop()
  end

  defp goodbye do
    IO.puts "Thank you for playing"
  end

  defp start_game(p1,p2) do
    {:ok, game} = ElixirDarts.Game.start_link(String.trim(p1),String.trim(p2))
    throw_darts(game)
  end

  defp throw_darts(game) do
    { p1, p2, player } = ElixirDarts.game(game)
    rest = get_rest(p1,p2, player)

    IO.puts "#{player} you need #{501 - rest}"

    case read_input() do
      {:invalid, error} -> IO.puts(error)
      {:ok, dart } -> ElixirDarts.throw(game, dart)
    end

    case ElixirDarts.game(game) do
      {_p1,_p2, {:winner, winner}} -> print_winner(winner)
      _ -> throw_darts(game)
    end
  end

  defp get_rest( {p1,d1}, _p2, p1 ) do
    Enum.reduce(d1, 0, fn({v,m}, sum) -> sum + v*m end)
  end
  defp get_rest( _p1, {p2,d2}, p2 ) do
    Enum.reduce(d2, 0, fn({v,m}, sum) -> sum + v*m end)
  end

  defp read_input do
    input = IO.gets("Enter SEGMENT MULTIPLIER: ")
            |> String.trim 
            |> String.split(~r/\s+/) 
            |> Enum.map(&String.trim(&1))

    try do
      [s,m] = input
      {seg,_} = Integer.parse(s)
      {mul,_} = Integer.parse(m)
      {:ok, {seg,mul}}
    rescue
      _e -> {:invalid, "INVALID ENTRY #{inspect input}, PLEASE TRY AGAIN"}
    end
  end


  defp print_winner(winner) do
    IO.puts "GAME SHOT, WINNER " <> winner
  end
end



