defmodule ElixirDarts.Game do
  use GenServer
  alias ElixirDarts.Rules

  # GenServer API

  def start_link(player_one, player_two) do
    GenServer.start_link(__MODULE__, {{player_one,[]},{player_two,[]}, player_one})
  end

  def init(state), do: {:ok, state}

  # Game API

  @doc"Return the state of the game"
  def game(pid) do
    GenServer.call(pid, :game)
  end

  @doc"Current player throws a dart"
  def throw_dart(pid, dart) do
    GenServer.cast(pid, {:throw, dart})
  end

  # GenServer Callbacks

  def handle_call(:game, _from, state) do
    {:reply, state, state}
  end

  def handle_cast( {:throw, dart}, {_p1,_p2, current_player} = state ) do
    {:noreply, state 
               |> Rules.count_for_current_player(current_player,dart) 
               |> Rules.dedect_bust        
               |> Rules.set_current_player
               |> Rules.detect_win       
    }
  end

end

